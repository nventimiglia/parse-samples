using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xamarin.Forms;
using XamParse.Views;
using Parse;


namespace XamParse
{
	public class App
	{

	    static App()
        {
            // Register sub objects
            ParseObject.RegisterSubclass<ParseScore>();

            // Initialize the parse client with your Application ID and .NET Key found on
            // your Parse dashboard
            ParseClient.Initialize("EeOwXTFyq3gXMVP76UUWBRairakqgKXW39dLIrbm", "WiSp7JPisAmPblL7cNVpxJauv6ZJBY6w9cNpoeAG");
	        
	    }

		public static Page GetMainPage()
		{
            return MasterPage.Get();
		}
	}
}

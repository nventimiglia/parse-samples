using Parse;

namespace XamParse
{

    /// <summary>
    /// Our example Score Model
    /// </summary>
    [ParseClassName("Score")]
    public class ParseScore : ParseObject
    {
        [ParseFieldName("Points")]
        public int Points
        {
            get { return GetProperty<int>(); }
            set { SetProperty(value); }
        }

        [ParseFieldName("UserName")]
        public string UserName
        {
            get { return GetProperty<string>(); }
            set { SetProperty(value); }
        }

        // TODO : Value Converter
        public string PointsConverted
        {
            get { return Points.ToString(); }
        }
    }
}
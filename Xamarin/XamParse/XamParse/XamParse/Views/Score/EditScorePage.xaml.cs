using System;
using System.Linq;
using Parse;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace XamParse.Views
{
    public partial class EditScorePage : ContentPage
    {
        protected static EditScorePage Instance { get; set; }

        public static EditScorePage Get()
        {
            if (Instance == null)
                Instance = new EditScorePage();
            return Instance;
        }

        public EditScorePage()
        {
            InitializeComponent();
            BindingContext = this;
        }



        private ParseScore _model;
        public ParseScore Model
        {
            get { return _model; }
            set
            {
                if (_model == value)
                    return;
                _model = value;

                Message = string.Empty;

                if (value == null)
                {
                    Points = "0";
                    UserName = string.Empty;

                }
                else
                {
                    Points = Model.PointsConverted;
                    UserName = Model.UserName;
                }

                OnPropertyChanged();
            }
        }

        

        private string _userName;
        public string UserName
        {
            get { return _userName; }
            set
            {
                if (_userName == value)
                    return;
                _userName = value;
                OnPropertyChanged();
            }
        }



        private string _points;
        public string Points
        {
            get { return _points; }
            set
            {
                if (_points == value)
                    return;
                _points = value;
                OnPropertyChanged();
            }
        }



        private string _message;
        public string Message
        {
            get { return _message; }
            set
            {
                if (_message == value)
                    return;
                _message = value;
                OnPropertyChanged();
            }
        }



        private Color _messageColor;
        public Color MessageColor
        {
            get { return _messageColor; }
            set
            {
                if (_messageColor == value)
                    return;
                _messageColor = value;
                OnPropertyChanged();
            }
        }


        public void Reset()
        {
            Message = string.Empty;
            Points = string.Empty;
            UserName = string.Empty;
        }

        public void Submit(object sender, EventArgs args)
        {
            SubmitAsync();
        }

        private async void SubmitAsync()
        {
            IsBusy = true;
            Message = string.Empty;
            try
            {

                int p = int.Parse(Points);

                Model.Points = p;
                Model.UserName = UserName;

                var task = Model.SaveAsync();

                await task;

                if (task.IsFaulted)
                {
                    Message = task.Exception.Message;
                    MessageColor = Color.Red;
                }
                else
                {
                    //reload
                    ListScoresPage.Get().OnSearch(null,null);

                    // Nav
                    MasterPage.Get().Detail = ListScoresPage.Get();
                }

            }
            catch (Exception ex)
            {
                Message = ex.Message;
                MessageColor = Color.Red;
            }

            IsBusy = false;

        }
    }
}

using System;
using System.Linq;
using  Parse;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace XamParse.Views
{
    public partial class AddScorePage : ContentPage
    {
        protected static AddScorePage Instance { get; set; }

        public static AddScorePage Get()
        {
            if (Instance == null)
                Instance = new AddScorePage();
            return Instance;
        }

        public AddScorePage()
        {
            InitializeComponent();
            BindingContext = this;
        }

        private string _userName;

        public string UserName
        {
            get { return _userName; }
            set
            {
                if (_userName == value)
                    return;
                _userName = value;
                OnPropertyChanged();
            }
        }



        private string _points;

        public string Points
        {
            get { return _points; }
            set
            {
                if (_points == value)
                    return;
                _points = value;
                OnPropertyChanged();
            }
        }



        private string _message;

        public string Message
        {
            get { return _message; }
            set
            {
                if (_message == value)
                    return;
                _message = value;
                OnPropertyChanged();
            }
        }



        private Color _messageColor;

        public Color MessageColor
        {
            get { return _messageColor; }
            set
            {
                if (_messageColor == value)
                    return;
                _messageColor = value;
                OnPropertyChanged();
            }
        }


        public void Reset()
        {
            Message = string.Empty;
            Points = string.Empty;
            UserName = string.Empty;
        }

        public void Submit(object sender, EventArgs args)
        {
            SubmitAsync();
        }

        private async void SubmitAsync()
        {
            IsBusy = true;
            Message = string.Empty;
            try
            {

                int p = int.Parse(Points);

                var model = new ParseScore
                {
                    UserName = UserName,
                    Points = p,
                };

                var task = model.SaveAsync();

                await task;

                if (task.IsFaulted)
                {
                    Message = task.Exception.Message;
                    MessageColor = Color.Red;
                }
                else
                {
                    //reload
                    ListScoresPage.Get().OnSearch(null, null);

                    // Nav
                    MasterPage.Get().Detail = ListScoresPage.Get();
                }

            }
            catch (Exception ex)
            {
                Message = ex.Message;
                MessageColor = Color.Red;
            }

            IsBusy = false;

        }
    }
}

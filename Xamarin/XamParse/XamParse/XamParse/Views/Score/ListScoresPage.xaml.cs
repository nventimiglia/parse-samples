using System;
using System.Collections.Generic;
using System.Linq;
using Parse;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace XamParse.Views
{
    public partial class ListScoresPage : ContentPage
    {
        protected static ListScoresPage Instance { get; set; }
        public static ListScoresPage Get()
        {
            if (Instance == null)
                Instance = new ListScoresPage();
            return Instance;
        }

        public ListScoresPage()
        {
            InitializeComponent();
            BindingContext = this;
        }


        private string _search;
        public string Search
        {
            get { return _search; }
            set
            {
                if (_search == value)
                    return;
                _search = value;
                OnPropertyChanged();
            }
        }

        private string _found = "0 Found";
        public string Found
        {
            get { return _found; }
            set
            {
                if (_found == value)
                    return;
                _found = value;
                OnPropertyChanged();
            }
        }



        private string _message;
        public string Message
        {
            get { return _message; }
            set
            {
                if (_message == value)
                    return;
                _message = value;
                ShowMessage = !string.IsNullOrEmpty(value);
                OnPropertyChanged();
            }
        }

        private bool _showList;
        public bool ShowList
        {
            get { return _showList; }
            set
            {
                if (_showList == value)
                    return;
                _showList = value;
                OnPropertyChanged();
            }
        }



        private bool _showMessage;
        public bool ShowMessage
        {
            get { return _showMessage; }
            set
            {
                if (_showMessage == value)
                    return;
                _showMessage = value;
                OnPropertyChanged();
            }
        }



        private IEnumerable<ParseScore> _items;
        public IEnumerable<ParseScore> Items
        {
            get { return _items; }
            set
            {
                if (_items == value)
                    return;
                _items = value;
                OnPropertyChanged();
            }
        }



        private ParseScore _selected;
        public ParseScore Selected
        {
            get { return _selected; }
            set
            {
                if (_selected == value)
                    return;
                _selected = value;
                OnPropertyChanged();
                if (value != null)
                    OnSelect(value);
            }
        }




        public void OnAdd(object sender, EventArgs e)
        {
            MasterPage.Get().Detail = AddScorePage.Get();
            AddScorePage.Get().Reset();
            MasterPage.Get().IsPresented = false;
        }

        public void OnSelect(ParseScore m)
        {
            var view = EditScorePage.Get();
            view.Model = m;
            MasterPage.Get().Detail = view;
        }

        public void OnSearch(object sender, EventArgs e)
        {
            SearchAsync(Search);
        }

        public async void SearchAsync(string filter)
        {
            IsBusy = true;
            ShowList = false;
            Found = string.Empty;
            Message = string.Empty;

            try
            {
                var query = new ParseQuery<ParseScore>();

                if (!string.IsNullOrEmpty(filter))
                {
                    query = query.WhereContains("UserName", filter);
                }

                query = query.OrderByDescending("Points");

                var task = query.FindAsync();

                await task;

                if (task.IsFaulted)
                {
                    Message = task.Exception.Message;
                }
                else
                {
                    Items = task.Result;
                    Found = string.Format("{0} Found", Items.Count());
                    ShowList = true;
                }

            }
            catch (Exception ex)
            {
                Message = ex.Message;
            }

            IsBusy = false;

        }

    }
}

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace XamParse.Views
{
	public partial class AboutPage : ContentPage
    {
        protected static AboutPage Instance { get; set; }
        public static AboutPage Get()
        {
            if (Instance == null)
                Instance = new AboutPage();
            return Instance;
        }

        public AboutPage()
		{
			InitializeComponent ();
		}
	}
}

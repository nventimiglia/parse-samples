using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace XamParse.Views
{
	public partial class MasterMenu : ContentPage
    {
        protected static MasterMenu Instance { get; set; }
        public static MasterMenu Get()
        {
            if (Instance == null)
                Instance = new MasterMenu();
            return Instance;
        }

        public MasterMenu()
		{
			InitializeComponent ();
		}

        public void OnAbout(object sender, EventArgs e)
        {
            MasterPage.Get().Detail = AboutPage.Get();
            MasterPage.Get().IsPresented = false;
        }

        public void OnStatus(object sender, EventArgs e)
        {
            MasterPage.Get().Detail = StatusPage.Get();
            MasterPage.Get().IsPresented = false;
        }

        public void OnSignUp(object sender, EventArgs e)
        {
            MasterPage.Get().Detail = SignUpPage.Get();
            MasterPage.Get().IsPresented = false;
        }
        public void OnSignIn(object sender, EventArgs e)
        {
            MasterPage.Get().Detail = SignInPage.Get();
            MasterPage.Get().IsPresented = false;

        }
        public void OnRecovery(object sender, EventArgs e)
        {
            MasterPage.Get().Detail = RecoveryPage.Get();
            MasterPage.Get().IsPresented = false;

        }
        public void OnScores(object sender, EventArgs e)
        {
            MasterPage.Get().Detail = ListScoresPage.Get();
            ListScoresPage.Get().SearchAsync(null);
            MasterPage.Get().IsPresented = false;

        }
        public void OnSignOut(object sender, EventArgs e)
        {
			Parse.ParseUser.LogOut ();

			// Show Status
			MasterPage.Get().Detail = StatusPage.Get();
			MasterPage.Get().IsPresented = false;
        }
	}
}

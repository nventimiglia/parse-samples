using Xamarin.Forms;

namespace XamParse.Views
{
	public partial class MasterPage : MasterDetailPage
    {
        protected static MasterPage Instance { get; set; }
        public static MasterPage Get()
	    {
	        if (Instance == null)
                Instance = new MasterPage();
	        return Instance;
	    }

        public MasterPage()
		{
			InitializeComponent ();
		}
	}
}

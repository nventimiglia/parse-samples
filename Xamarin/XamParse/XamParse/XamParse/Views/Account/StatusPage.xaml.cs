using Xamarin.Forms;
using Parse;

namespace XamParse.Views
{
    public partial class StatusPage : ContentPage
    {
        protected static StatusPage Instance { get; set; }
        public static StatusPage Get()
        {
            if (Instance == null)
                Instance = new StatusPage();
            Instance.LoadAccountStatus();
            return Instance;
        }

        public StatusPage()
        {
            InitializeComponent();
            BindingContext = this;
        }

        public void ShowBusy()
        {
            StatusColor = Color.Black;
            StatusText = "Loading...";
        }


        private Color _color;
        public Color StatusColor
        {
            get { return _color; }
            set
            {
                if (_color == value)
                    return;
                _color = value;
                OnPropertyChanged();
            }
        }

        private string _status;
        public string StatusText
        {
            get { return _status; }
            set
            {
                if (_status == value)
                    return;
                _status = value;
                OnPropertyChanged();
            }
        }


        /// <summary>
        /// Loads data from Parse
        /// </summary>
        public void LoadAccountStatus()
        {
            if (ParseUser.CurrentUser == null || !ParseUser.CurrentUser.IsAuthenticated)
            {
                StatusText = "Not Authenticated";
                StatusColor = Color.Red;
            }
            else
            {
                StatusText = "Hello, " + ParseUser.CurrentUser.Username;
                StatusColor = Color.Green;
            }
        }
    }
}

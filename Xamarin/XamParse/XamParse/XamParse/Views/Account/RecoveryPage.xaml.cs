using System;
using System.Linq;
using Xamarin.Forms;
using Parse;

namespace XamParse.Views
{
    public partial class RecoveryPage : ContentPage
    {
        protected static RecoveryPage Instance { get; set; }
        public static RecoveryPage Get()
        {
            if (Instance == null)
                Instance = new RecoveryPage();
            return Instance;
        }
        public RecoveryPage()
        {
            InitializeComponent();
        }

		private string _email;
		public string Email
		{
			get { return _email; }
			set
			{
				if (_email == value)
					return;
				_email = value;
				OnPropertyChanged();
			}
		}


		private string _message;
		public string Message
		{
			get { return _message; }
			set
			{
				if (_message == value)
					return;
				_message = value;
				OnPropertyChanged();
			}
		}
        
        private Color _messageColor;
        public Color MessageColor
        {
            get { return _messageColor; }
            set
            {
                if (_messageColor == value)
                    return;
                _messageColor = value;
                OnPropertyChanged();
            }
        }

        

		public void Submit(object sender, EventArgs args)
		{
			SubmitAsync();
		}

		async void SubmitAsync()
		{
			IsBusy = true;
			Message = string.Empty;


            try
            {
                var task = ParseUser.RequestPasswordResetAsync(Email);

                await task;

                IsBusy = false;

                if (task.IsFaulted)
                {
                    Message = task.Exception.Message;
                    MessageColor = Color.Red;
                }
                else
                {
                    Message = "Recovery Sent, Thank You!";
                    MessageColor = Color.Green;
                }
            }
            catch (Exception ex)
            {
                Message = ex.Message;
                MessageColor = Color.Red;
            }

            IsBusy = false;
		}
	}
}

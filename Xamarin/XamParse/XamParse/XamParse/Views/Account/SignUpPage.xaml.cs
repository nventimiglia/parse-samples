using System;
using System.Linq;
using Xamarin.Forms;
using Parse;

namespace XamParse.Views
{
    public partial class SignUpPage : ContentPage
    {
        protected static SignUpPage Instance { get; set; }
        public static SignUpPage Get()
        {
            if (Instance == null)
                Instance = new SignUpPage();
            return Instance;
        }

        public SignUpPage()
        {
            InitializeComponent();
            BindingContext = this;
        }


        private string _userName;
        public string UserName
        {
            get { return _userName; }
            set
            {
                if (_userName == value)
                    return;
                _userName = value;
                OnPropertyChanged();
            }
        }



        private string _password;
        public string Password
        {
            get { return _password; }
            set
            {
                if (_password == value)
                    return;
                _password = value;
                OnPropertyChanged();
            }
        }

        private string _email;
        public string Email
        {
            get { return _email; }
            set
            {
                if (_email == value)
                    return;
                _email = value;
                OnPropertyChanged();
            }
        }




        private string _message;
        public string Message
        {
            get { return _message; }
            set
            {
                if (_message == value)
                    return;
                _message = value;
                OnPropertyChanged();
            }
        }

        public void Submit(object sender, EventArgs args)
        {
            SubmitAsync();
        }

        async void SubmitAsync()
        {
            IsBusy = true;
            Message = string.Empty;

            try
            {
                var user = new ParseUser
                {
                    Username = UserName,
                    Email = Email,
                    Password = Password
                };

                var task = user.SignUpAsync();

                await task;

                if (task.IsFaulted)
                {
                    Message = task.Exception.Message;
                }
                else
                {
                    MasterMenu.Get().OnStatus(this, null);
                }
            }
            catch (Exception ex)
            {
                Message = ex.Message;
            }

            IsBusy = false;

        }
    }
}

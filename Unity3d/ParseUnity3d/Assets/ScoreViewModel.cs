using System;
using System.Collections;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class ScoreViewModel : MonoBehaviour
{
    public Text NameLabel;
    public Text ScoreLabel;

    public ParseScore Model;
    public DemoViewModel Parent;

    public void Bind(DemoViewModel parent, ParseScore model)
    {
        NameLabel.text = model.UserName;
        ScoreLabel.text = model.Points.ToString();
        Model = model;
        Parent = parent;
    }

    public void Select()
    {
        Parent.Select(this);
    }
}
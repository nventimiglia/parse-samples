using Parse;

/// <summary>
/// Our example Score Model
/// </summary>
[ParseClassName("Score")]
public class ParseScore : ParseObject
{
    [ParseFieldName("Points")]
    public int Points
    {
        get { return GetProperty<int>("Points"); }
        set { SetProperty(value, "Points"); }
    }

    [ParseFieldName("UserName")]
    public string UserName
    {
        get { return GetProperty<string>("UserName"); }
        set { SetProperty(value, "UserName"); }
    }
}
using System;
using System.Collections.Generic;
using System.Linq;
using Parse;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public class DemoViewModel : MonoBehaviour
{
    public Color NormalState = Color.black;
    public Color SuccessState = Color.black;
    public Color ErrorState = Color.black;


    public GameObject AccountNotice;
    public GameObject BusyModel;
    public GameObject[] Views;

    public Text TitleLabel;
    public Text StateLabel;
    public InputField NameField;
    public InputField PasswordField;
    public InputField NameField2;
    public InputField PasswordField2;
    public InputField EmailField2;
    public InputField EmailField3;

    public InputField SearchField;
    public Text FoundLabel;
    public GameObject ItemTemplate;
    public GameObject ListView;


    public InputField AddNameField;
    public InputField AddPointsField;

    public InputField EditNameField;
    public InputField EditPointsField;

    protected ScoreViewModel Selected;
    protected List<GameObject> Items = new List<GameObject>();

    void Start()
    {
        // Register sub objects
        ParseObject.RegisterSubclass<ParseScore>();
        
        ItemTemplate.SetActive(false);
        StateLabel.text = string.Empty;
        SetAccountNotice();
        NavMain();
    }

    public void SetState(string txt)
    {
        StateLabel.text = txt;
    }
    public void SetState(Color c)
    {
        StateLabel.color = c;
    }

    #region navigation

    void NavigateTo(string arg)
    {
        foreach (var view in Views)
        {
            view.SetActive(arg == view.name);
        }

        Selected = null;
        TitleLabel.text = arg;
    }

    public void NavMain()
    {
        NavigateTo("Main");
    }

    public void NavSignIn()
    {
        NavigateTo("SignIn");
    }

    public void NavSignUp()
    {
        NavigateTo("SignUp");
    }

    public void NavRecovery()
    {
        NavigateTo("Recovery");
    }

    public void NavListScores()
    {
        NavigateTo("ScoresList");
        StartCoroutine(Load(string.Empty));
    }

    public void NavAddScore()
    {
        NavigateTo("AddScore");
    }

    public void NavAbout()
    {
        NavigateTo("About");
    }

    #endregion

    #region Account Methods

    void SetAccountNotice()
    {
        AccountNotice.SetActive(ParseUser.CurrentUser != null);
    }

    public void SubmitSignIn()
    {
        StartCoroutine(SignInAsync());
    }

    IEnumerator SignInAsync()
    {
        SetState("Signing In...");
        SetState(NormalState);

        BusyModel.SetActive(true);
        var task = ParseUser.LogInAsync(NameField.value, PasswordField.value);

        while (!task.IsCompleted)
        {
            yield return 1;
        }
        BusyModel.SetActive(false);

        if (task.IsFaulted)
        {
            var exception = (ParseException) task.Exception.InnerExceptions.First();

            //Code is "OtherCauses"
            SetState(exception.Message);
            SetState(ErrorState);
        }
        else
        {
            SetState("Welcome, " + task.Result.Username);
            SetState(SuccessState);
            SetAccountNotice();
        }
    }

    public void SubmitSignUp()
    {
        StartCoroutine(SignUpAsync());
    }

    IEnumerator SignUpAsync()
    {
        SetState("Signing Up...");
        SetState(NormalState);

        var user = new ParseUser
        {
            Username = NameField2.value,
            Email = EmailField2.value,
            Password = PasswordField2.value,
        };


        BusyModel.SetActive(true);
        var task = user.SignUpAsync();

        while (!task.IsCompleted)
        {
            yield return 1;
        }
        BusyModel.SetActive(false);

        if (task.IsFaulted)
        {
            var exception = (ParseException)task.Exception.InnerExceptions.First();

            SetState(exception.Message);
            SetState(ErrorState);
        }
        else
        {
            SetState("Welcome, " + user.Username);
            SetState(SuccessState);
            SetAccountNotice();
        }
    }

    public void SubmitRecovery()
    {
        StartCoroutine(RecoveryAsync());
    }

    IEnumerator RecoveryAsync()
    {
        SetState("Recovery...");
        SetState(NormalState);

        BusyModel.SetActive(true);
        var task = ParseUser.RequestPasswordResetAsync(EmailField3.value);

        while (!task.IsCompleted)
        {
            yield return 1;
        }
        BusyModel.SetActive(false);

        if (task.IsFaulted)
        {
            var exception = (ParseException)task.Exception.InnerExceptions.First();

            SetState(exception.Message);
            SetState(ErrorState);
        }
        else
        {
            SetState("Email sent to " + EmailField3.value);
            SetState(SuccessState);
        }
    }

    public void PrintAccountStatus()
    {
        if (ParseUser.CurrentUser == null)
        {
            SetState("You are not signed in");
            SetState(NormalState);
        }
        else
        {
            SetState("Hello, " + ParseUser.CurrentUser.Username);
            SetState(NormalState);
        }
    }

    public void SubmitSignOut()
    {
        if (ParseUser.CurrentUser == null)
        {
            SetState("You are not signed in");
            SetState(NormalState);
        }
        else
        {
            ParseUser.LogOut();
            SetState("Signed out");
            SetState(NormalState);
            SetAccountNotice();
        }
    }
    #endregion

    #region scores

    public void SubmitSearch()
    {
        StartCoroutine(Load(SearchField.value));
    }

    public IEnumerator Load(string filter)
    {
        SetState("Loading Scores...");
        SetState(NormalState);

        BusyModel.SetActive(true);

        var quey = new ParseQuery<ParseScore>();

        if (!string.IsNullOrEmpty(filter))
        {
            quey = quey.WhereContains("UserName", filter);
        }

        quey = quey.OrderByDescending("Points");

        var task = quey.FindAsync();

        while (!task.IsCompleted)
        {
            yield return 1;
        }

        //clear old items
        foreach (var item in Items)
        {
            Destroy(item);
        }

        yield return 1;

        if (task.IsFaulted)
        {
            var exception = task.Exception.InnerExceptions.First();

            SetState(exception.Message);
            SetState(ErrorState);
        }
        else
        {
            // count
            var count = task.Result.Count();
            FoundLabel.text = string.Format("{0} Found", count);
            SetState(FoundLabel.text);
            SetState(SuccessState);

            // make items
            foreach (var item in task.Result)
            {
                var go = (GameObject)Instantiate(ItemTemplate);
                var vm = go.GetComponent<ScoreViewModel>();
                vm.Bind(this, item);

                go.transform.parent = ListView.transform;
                go.SetActive(true);

                Items.Add(go);
            }
        }
        BusyModel.SetActive(false);
    }

    public void SubmitAdd()
    {
        StartCoroutine(AddAsync());
    }

    IEnumerator AddAsync()
    {
        ParseScore model;

        try
        {
            model = new ParseScore
            {
                UserName = AddNameField.value,
                Points = int.Parse(AddPointsField.value)
            };

            // set unprotected ACL
            model.ACL = new ParseACL(ParseUser.CurrentUser) { PublicReadAccess = true, PublicWriteAccess = true };

        }
        catch (Exception ex)
        {
            SetState(ErrorState);
            SetState(ex.Message);
            yield break;
        }


        SetState("Posting Score...");
        SetState(NormalState);
        BusyModel.SetActive(true);

        var task = model.SaveAsync();

        while (!task.IsCompleted)
        {
            yield return 1;
        }

        if (task.IsFaulted)
        {
            var exception = task.Exception.InnerExceptions.First();

            SetState(exception.Message);
            SetState(ErrorState);
        }
        else
        {
            // count
            SetState("Score Posted");
            SetState(SuccessState);
        }
        BusyModel.SetActive(false);
    }

    public void Select(ScoreViewModel model)
    {
        NavigateTo("EditScore");
        Selected = model;
        EditNameField.value = model.Model.UserName;
        EditPointsField.value = model.Model.Points.ToString();
    }

    public void SubmitEdit()
    {
        StartCoroutine(EditAsync());
    }

    IEnumerator EditAsync()
    {
        try
        {
            Selected.Model.UserName = EditNameField.value;
            Selected.Model.Points = int.Parse(EditPointsField.value);

        }
        catch (Exception ex)
        {
            SetState(ErrorState);
            SetState(ex.Message);
            yield break;
        }


        SetState("Posting Score...");
        SetState(NormalState);
        BusyModel.SetActive(true);

        var task = Selected.Model.SaveAsync();

        while (!task.IsCompleted)
        {
            yield return 1;
        }

        if (task.IsFaulted)
        {
            var exception = task.Exception.InnerExceptions.First();

            SetState(exception.Message);
            SetState(ErrorState);
        }
        else
        {
            // count
            SetState("Score Posted");
            SetState(SuccessState);
        }
        BusyModel.SetActive(false);

        NavListScores();
    }
    #endregion

}

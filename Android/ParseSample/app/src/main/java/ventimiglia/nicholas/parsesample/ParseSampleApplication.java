package ventimiglia.nicholas.parsesample;
import android.app.Application;
import com.parse.Parse;
import com.parse.ParseACL;
import com.parse.ParseObject;
import com.parse.ParseUser;

public class ParseSampleApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        // Init
        Parse.initialize(this, "EeOwXTFyq3gXMVP76UUWBRairakqgKXW39dLIrbm", "RYOENigWRlT7wUPNEXLN9UNXBEBq9rcbuEEgQp0H");

        // Register Subclasses
        ParseObject.registerSubclass(Score.class);

        // Default Security
        ParseACL defaultACL = new ParseACL();

        // note : dont use public r/w in production
        defaultACL.setPublicReadAccess(true);
        defaultACL.setPublicWriteAccess(true);
        ParseACL.setDefaultACL(defaultACL, true);
    }
}
package ventimiglia.nicholas.parsesample;

import android.app.ActionBar;
import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SignUpCallback;


public class SignInFragment extends Fragment {

    // <editor-fold defaultstate="collapsed" desc="Default Members">
    Activity mParent;
    View root;

    public static SignInFragment newInstance() {
        SignInFragment fragment = new SignInFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }
    public SignInFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        mParent.setTitle("Sign In");

        View view = inflater.inflate(R.layout.fragment_sign_in, container, false);

        final Button button = (Button) view.findViewById(R.id.btnSubmit);

        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                submit(v);
            }
        });

        root = view;

        return view;
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mParent = activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mParent = null;
    }

    // </editor-fold>

    public void  submit(View view){

        // Resolve stuff
        final Button button = (Button) root.findViewById(R.id.btnSubmit);
        final String username = ((EditText) root.findViewById(R.id.txtUsername)).getText().toString();
        final String password = ((EditText) root.findViewById(R.id.txtPassword)).getText().toString();
        final ProgressBar progress = (ProgressBar) root.findViewById(R.id.spinner);

        // Validate
        if(password == null || password.isEmpty()){
            Toast.makeText(super.getActivity(), "Invalid Password", Toast.LENGTH_SHORT).show();
            return;
        }
        if(username == null || username.isEmpty()){
            Toast.makeText(super.getActivity(), "Invalid Username", Toast.LENGTH_SHORT).show();
            return;
        }

        //UI
        Toast.makeText(super.getActivity(), "Signing In..", Toast.LENGTH_SHORT).show();
        progress.setVisibility(View.VISIBLE);

        ParseUser.logInInBackground(username, password, new LogInCallback() {
            public void done(ParseUser user, ParseException e) {
                progress.setVisibility(View.INVISIBLE);
                if (user != null) {
                    Toast.makeText(mParent, "Hooray! Welcome, "+user.getUsername(), Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(mParent, e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        });
    }
}

package ventimiglia.nicholas.parsesample;

import android.app.ActionBar;
import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.RequestPasswordResetCallback;

public class RecoveryFragment extends Fragment {

    // <editor-fold defaultstate="collapsed" desc="Default Members">
    Activity mParent;
    View root;

    public static RecoveryFragment newInstance() {
        RecoveryFragment fragment = new RecoveryFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }
    public RecoveryFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        mParent.setTitle("Account Recovery");

        View view = inflater.inflate(R.layout.fragment_recovery, container, false);

        final Button button = (Button) view.findViewById(R.id.btnSubmit);

        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                submit(v);
            }
        });

        root = view;

        return view;
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mParent = activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mParent = null;
    }

    // </editor-fold>

    public void  submit(View view){

        // Resolve stuff
        final String email = ((EditText) root.findViewById(R.id.txtEmail)).getText().toString();
        final ProgressBar progress = (ProgressBar) root.findViewById(R.id.spinner);

        // Validate
        if(email == null || email.isEmpty()){
            Toast.makeText(super.getActivity(), "Invalid Email", Toast.LENGTH_SHORT).show();
            return;
        }

        //UI
        Toast.makeText(super.getActivity(), "Recovering..", Toast.LENGTH_SHORT).show();
        progress.setVisibility(View.VISIBLE);

        ParseUser.requestPasswordResetInBackground(email,
                new RequestPasswordResetCallback() {
                    public void done(ParseException e) {
                        progress.setVisibility(View.INVISIBLE);
                        if (e == null) {
                            Toast.makeText(mParent, "Hooray! Recovery Sent to : " + email, Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(mParent, e.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    }
                });
    }
}

package ventimiglia.nicholas.parsesample;

import android.app.ActionBar;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.parse.SignUpCallback;

public class ScoreEditFragment extends Fragment {


    // <editor-fold defaultstate="collapsed" desc="Default Members">
    Activity mParent;
    View root;
    Score model;

    public static ScoreEditFragment newInstance(Score m) {
        ScoreEditFragment fragment = new ScoreEditFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        fragment.model = m;
        return fragment;
    }
    public ScoreEditFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        mParent.setTitle(String.format("Edit %s", model.getUserName()));

        View view = inflater.inflate(R.layout.fragment_score_edit, container, false);

        final Button button = (Button) view.findViewById(R.id.btnSubmit);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                submit(v);
            }
        });

        final EditText txtName = (EditText) view.findViewById(R.id.txtUsername);
        final EditText txtScore = (EditText) view.findViewById(R.id.txtPoints);

        txtName.setText(model.getUserName());
        txtScore.setText(Integer.toString(model.getPoints()));

        root = view;

        return view;
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mParent = activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mParent = null;
    }

    // </editor-fold>

    public void  submit(View view){

        // Resolve stuff
        final String username = ((EditText) root.findViewById(R.id.txtUsername)).getText().toString();
        final String score = ((EditText) root.findViewById(R.id.txtPoints)).getText().toString();
        final ProgressBar progress = (ProgressBar) root.findViewById(R.id.spinner);


        // Validate
        if(score == null || score.isEmpty()){
            Toast.makeText(super.getActivity(), "Invalid Score", Toast.LENGTH_SHORT).show();
            return;
        }
        if(username == null || username.isEmpty()){
            Toast.makeText(super.getActivity(), "Invalid Username", Toast.LENGTH_SHORT).show();
            return;
        }

        int points = 0;

        try {
            points = Integer.parseInt(score);
        } catch(NumberFormatException nfe) {
            Toast.makeText(super.getActivity(), "Invalid Score", Toast.LENGTH_SHORT).show();
            return;
        }

        //UI
        Toast.makeText(super.getActivity(), "Posting Score..", Toast.LENGTH_SHORT).show();
        progress.setVisibility(View.VISIBLE);

        model.setPoints(points);
        model.setUserName(username);

        model.saveInBackground(
                new SaveCallback() {
                    public void done(com.parse.ParseException e) {
                        progress.setVisibility(View.INVISIBLE);
                        if (e == null) {
                            Toast.makeText(mParent, "Hooray! Score Saved", Toast.LENGTH_LONG).show();

                            //Return to List
                            FragmentManager fragmentManager = getFragmentManager();
                            fragmentManager.beginTransaction()
                                    .replace(R.id.container, ScoresListFragment.newInstance())
                                    .commit();

                        } else {
                            Toast.makeText(mParent, e.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    }
                });
    }
}

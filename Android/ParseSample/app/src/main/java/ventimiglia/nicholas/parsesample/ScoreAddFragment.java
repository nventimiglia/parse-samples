package ventimiglia.nicholas.parsesample;

import android.app.ActionBar;
import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.parse.SignUpCallback;

public class ScoreAddFragment extends Fragment {


    // <editor-fold defaultstate="collapsed" desc="Default Members">
    Activity mParent;
    View root;

    public static ScoreAddFragment newInstance() {
        ScoreAddFragment fragment = new ScoreAddFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }
    public ScoreAddFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        mParent.setTitle("Add Score");

        View view = inflater.inflate(R.layout.fragment_score_add, container, false);

        final Button button = (Button) view.findViewById(R.id.btnSubmit);

        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                submit(v);
            }
        });

        root = view;

        return view;
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mParent = activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mParent = null;
    }

    // </editor-fold>

    public void  submit(View view){

        // Resolve stuff
        final String username = ((EditText) root.findViewById(R.id.txtUsername)).getText().toString();
        final String score = ((EditText) root.findViewById(R.id.txtPoints)).getText().toString();
        final ProgressBar progress = (ProgressBar) root.findViewById(R.id.spinner);


        // Validate
        if(score == null || score.isEmpty()){
            Toast.makeText(super.getActivity(), "Invalid Score", Toast.LENGTH_SHORT).show();
            return;
        }
        if(username == null || username.isEmpty()){
            Toast.makeText(super.getActivity(), "Invalid Username", Toast.LENGTH_SHORT).show();
            return;
        }

        int points = 0;

        try {
            points = Integer.parseInt(score);
        } catch(NumberFormatException nfe) {
            Toast.makeText(super.getActivity(), "Invalid Score", Toast.LENGTH_SHORT).show();
            return;
        }

        //UI
        Toast.makeText(super.getActivity(), "Posting Score..", Toast.LENGTH_SHORT).show();
        progress.setVisibility(View.VISIBLE);

        Score dto = new Score();
        dto.setPoints(points);
        dto.setUserName(username);


        dto.saveInBackground(
                new SaveCallback() {
                    public void done(com.parse.ParseException e) {
                        progress.setVisibility(View.INVISIBLE);
                        if (e == null) {
                            Toast.makeText(mParent, "Hooray! Score Saved", Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(mParent, e.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    }
                });
    }
}

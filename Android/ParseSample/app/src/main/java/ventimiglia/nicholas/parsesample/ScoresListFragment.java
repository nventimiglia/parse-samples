package ventimiglia.nicholas.parsesample;

import android.app.ActionBar;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Loader;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;

import java.util.List;


public class ScoresListFragment extends Fragment {


    // <editor-fold defaultstate="collapsed" desc="Boiler Plate">
    Activity mParent;
    View root;
    ScoreAdapter adapter;

    public static ScoresListFragment newInstance() {
        ScoresListFragment fragment = new ScoresListFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    public ScoresListFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                            Bundle savedInstanceState) {

        mParent.setTitle("List Scores");

        View view = inflater.inflate(R.layout.fragment_score_list, container, false);

        final Button add = (Button) view.findViewById(R.id.btnAdd);
        add.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                add(v);
            }
        });

        final Button submit = (Button) view.findViewById(R.id.btnSubmit);
        submit.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                submit(v);
            }
        });

        final ListView list = (ListView) view.findViewById(R.id.lstScores);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                                    long arg3) {


               Object model = adapter.getItem(arg2);

                FragmentManager fragmentManager = getFragmentManager();
                fragmentManager.beginTransaction()
                        .replace(R.id.container, ScoreEditFragment.newInstance((Score) model))
                        .commit();

            }
        });

        root = view;

        load(null);

        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mParent = activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mParent = null;
    }

    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Adapter">

    public class ScoreAdapter extends BaseAdapter {

        List<Score> Context;
        Activity activity;

        public ScoreAdapter(List<Score> data, Activity act){
            Context = data;
            activity = act;
        }

        @Override
        public int getCount() {
            return Context.size();
        }

        @Override
        public Object getItem(int arg0) {
            return Context.get(arg0);
        }

        @Override
        public long getItemId(int arg0) {
            return arg0;
        }

        @Override
        public View getView(int arg0, View arg1, ViewGroup arg2) {

            if(arg1==null)
            {
                LayoutInflater inflater = LayoutInflater.from(activity);
                arg1 = inflater.inflate(R.layout.fragment_score_item, arg2,false);
            }

            TextView txtName = (TextView)arg1.findViewById(R.id.txtName);
            TextView txtScore = (TextView)arg1.findViewById(R.id.txtScore);

            Score s = Context.get(arg0);

            txtName.setText(s.getUserName());
            txtScore.setText(Integer.toString(s.getPoints()));

            return arg1;
        }
    }

    // </editor-fold>

    public void  load(String filter){

        // Get controls
        final TextView txtCount = (TextView) root.findViewById(R.id.txtCount);
        final ProgressBar progress = (ProgressBar) root.findViewById(R.id.spinner);
        final ListView list = (ListView) root.findViewById(R.id.lstScores);


        // Create Query
        ParseQuery<Score> query = ParseQuery.getQuery(Score.class);

        // Apply Filter
        if(filter != null && !filter.isEmpty()){
            query.whereContains("UserName",filter);
        }

        // Sort
        query.orderByDescending("Points");

        //UI
        progress.setVisibility(View.VISIBLE);
        list.setVisibility(View.GONE);

        query.findInBackground(new FindCallback<Score>() {
            @Override
            public void done(List<Score> results, ParseException e) {

                //UI
                progress.setVisibility(View.GONE);

                // check for errors
                if(e != null){
                    Toast.makeText(mParent, e.getMessage(), Toast.LENGTH_LONG);
                    txtCount.setText("Error !");
                }else
                {
                    list.setVisibility(View.VISIBLE);

                    //print count
                    txtCount.setText(results.size()+ " Found");

                    adapter = new ScoreAdapter(results, mParent);
                    list.setAdapter(adapter);
                }
            }
        });
    }

    public void  submit(View view){

        // Get Filter
        final EditText txtInput = (EditText) root.findViewById(R.id.txtInput);
        String filter = txtInput.getText().toString();

        // Hide Keyboard
        InputMethodManager imm = (InputMethodManager)mParent.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(txtInput.getWindowToken(), 0);

        load(filter);
    }

    public void  add(View view){
        FragmentManager fragmentManager = getFragmentManager();
                fragmentManager.beginTransaction()
                        .replace(R.id.container, ScoreAddFragment.newInstance())
                        .commit();
    }

    public void edit(View view){

    }
}

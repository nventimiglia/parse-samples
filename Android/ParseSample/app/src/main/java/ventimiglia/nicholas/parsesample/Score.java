package ventimiglia.nicholas.parsesample;

import com.parse.ParseClassName;
import com.parse.ParseObject;

@ParseClassName("Score")
public class Score extends ParseObject {

    // Username
    public String getUserName() {
        return getString("UserName");
    }
    public void setUserName(String value) {
        put("UserName", value);
    }

    // Points
    public int getPoints() {
        return getInt("Points");
    }

    public void setPoints(int value) {
        put("Points", value);
    }
}
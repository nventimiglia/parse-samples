package ventimiglia.nicholas.parsesample;

import android.app.ActionBar;
import android.app.Activity;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.parse.ParseUser;
import com.parse.SignUpCallback;

import java.text.ParseException;


public class SignUpFragment extends Fragment {

    // <editor-fold defaultstate="collapsed" desc="Default Members">

    Activity mParent;
    View root;

    public static SignUpFragment newInstance() {
        SignUpFragment fragment = new SignUpFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }
    public SignUpFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        mParent.setTitle("Sign Up");

        View view = inflater.inflate(R.layout.fragment_sign_up, container, false);

        final Button button = (Button) view.findViewById(R.id.btnSubmit);

        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                submit(v);
            }
        });

        root = view;
        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mParent = activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mParent = null;
    }
    // </editor-fold>

    public void  submit(View view){

        // Resolve stuff
        final Button button = (Button) root.findViewById(R.id.btnSubmit);
        final String username = ((EditText) root.findViewById(R.id.txtUsername)).getText().toString();
        final String password = ((EditText) root.findViewById(R.id.txtPassword)).getText().toString();
        final String email = ((EditText) root.findViewById(R.id.txtEmail)).getText().toString();
        final ProgressBar progress = (ProgressBar) root.findViewById(R.id.spinner);

        // Validate
        if(email == null || email.isEmpty()){
            Toast.makeText(super.getActivity(), "Invalid Email", Toast.LENGTH_SHORT).show();
            return;
        }
        if(password == null || password.isEmpty()){
            Toast.makeText(super.getActivity(), "Invalid Password", Toast.LENGTH_SHORT).show();
            return;
        }
        if(username == null || username.isEmpty()){
            Toast.makeText(super.getActivity(), "Invalid Username", Toast.LENGTH_SHORT).show();
            return;
        }

        //UI
        Toast.makeText(super.getActivity(), "Signing Up..", Toast.LENGTH_SHORT).show();
        progress.setVisibility(View.VISIBLE);

        ParseUser user = new ParseUser();
        user.setEmail(email);
        user.setPassword(password);
        user.setUsername(username);


        user.signUpInBackground(
                new SignUpCallback() {
                    public void done(com.parse.ParseException e) {
                        progress.setVisibility(View.INVISIBLE);
                        if (e == null) {
                            Toast.makeText(mParent, "Hooray! Welcome, "+username, Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(mParent, e.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    }
                });
    }
}
